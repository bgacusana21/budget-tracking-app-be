const mongoose = require('mongoose')
const moment = require('moment')

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'First name is required']
    },
    lastName: {
        type: String,
        required: [true, 'Last name is required']
    },
    mobileNo: {
        type: String,
        //required: [true, 'Mobile number is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required']
    },
    loginType: {
        type: String,
        required: [true, 'login type is required']
    },
    password: {
        type: String,
        //required: [true, 'Password is required']
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    userStatus: {
        type: String,
        default: 'Active'
    },
    transaction: [
        {
            transactionType: {
                type: String,
                required: [true, 'Transaction type is required']
            },
            transactSubCat: {
                type: String,
                required: [true, 'Transaction subcatetory is required']
            },
            summary: {
                type: String,
                required: [true, 'Summary is required']
            },
            description: {
                type: String
            },
            amount: {
                type: Number,
                default: 0
            },
            transactionTimestamp: {
                type: Date,
                required: [true, 'Date is required']
            }
        }
    ]
})

module.exports = mongoose.model('user', userSchema)