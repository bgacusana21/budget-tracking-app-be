const mongoose = require('mongoose')

const transactionCategorySchema = new mongoose.Schema({
    userRef: {
        type: String,
        required: [true, 'category type is required']
    }, 
    categoryType: {
        type: String,
        required: [true, 'category type is required']
    },
    name: {
        type: String,
        required: [true, 'name is required']
    },
    value: {
        type: String,
        required: [true, 'value is required']
    },
    isActive: {
        type: Boolean,
        default: true
    },
    isDebit: {
        type: Boolean,
        required: [true, 'transaction type is required']
    },
    categorySelection: [
        {
            categoryType: {
                type: String,
                required: [true, 'category type is required']
            },
            name: {
                type: String,
                required: [true, 'name is required']
            },
            value: {
                type: String,
                required: [true, 'value is required']
            },
            isActive: {
                type: Boolean,
                default: true
            }
            
        }
    ]
})

module.exports = mongoose.model('transactionCategory', transactionCategorySchema)