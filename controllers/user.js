const User = require('../models/User')
const auth = require('../auth')
const bcrypt = require('bcrypt')
const { OAuth2Client } = require ('google-auth-library')

const clientId = '907217793776-nvnkmjhlpiv7fjkun25vhbo5mjnsv6ga.apps.googleusercontent.com'

module.exports.register = (params) => {
    let user = new User({
        firstName: params.firstName,
        lastName: params.lastName,
        email: params.email,
        loginType: "web",
        mobileNo: params.mobileNo,
        password: bcrypt.hashSync(params.password, 10)
    })

    return user.save().then((user, err) => {
        return (err) ? false : true
    })
}

module.exports.emailExists = (params) => {
    //find a user document with matching email
    return User.find({ email: params.email })
    .then(result => {
        //if match found, return true
		return result.length > 0 ? true : false
    })
    .catch(err => console.log(err))
}

module.exports.login = (params) => {
    return User.findOne({ email: params.email}).then( user => {
        if (user === null) {
            return { error: 'does-not-exist'}
        }
        if (user.loginType !== 'web') {
            return { error: 'login-type-error'}
        }

        const isPasswordMatched = bcrypt.compareSync(params.password, user.password)
        if (isPasswordMatched) {
            return { accessToken: auth.createAccessToken(user.toObject())}
        } else {
            return { error: 'incorrect-password'}
        }

    })
}

module.exports.change = (params) => {
    let update = { password: bcrypt.hashSync(params.newPassword, 10)}

    return User.findById(params.userRef).then(result => { 
        let isPasswordMatched = bcrypt.compareSync(params.oldPassword, result.password)
        
        if (isPasswordMatched) {
            return User.findByIdAndUpdate(params.userRef, update).then((doc, err) => {
                return (err) ? false : true
            })
        } else {
            return false
        }
    })
}

module.exports.verifyGoogleTokenId = async (params) => {
    const client = new OAuth2Client(clientId)
    const data = await client.verifyIdToken({
        idToken: params,
        audience: clientId
    })

    if(data.payload.email_verified){
        const user = await User.findOne({ email: data.payload.email}).exec()
        
        if (user !== null ){
            if (user.loginType === "google") {
                return {accessToken:auth.createAccessToken(user.toObject())}
            } else {
                return { error: "login-type-error"}
            }
        } else {
            const newUser = new User({
                firstName: data.payload.given_name,
                lastName: data.payload.family_name,
                email: data.payload.email,
                loginType: "google"
            })

            return newUser.save().then((user, err) => {
                return {
                    accessToken: auth.createAccessToken(user.toObject())
                }
            })
        }
    } else {
        return { error: "google-auth-error"}
    }
}

module.exports.get = (params) => {
    return User.findById(params.userId).then(user => {
        user.password = undefined
        return user
    })
}

module.exports.addTransaction = (params) => {
    return User.findById(params.userRef).then(result => {
        result.transaction.push(params.body)
        return result.save().then((data, err) => {
            return (err) ? false : true
        })
    })
}

module.exports.transactions = (params) => {
    return User.findById(params.userRef).then(result => {
        return result.transaction
    })
}

