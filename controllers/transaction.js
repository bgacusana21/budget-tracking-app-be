const Transaction = require('../models/Transaction')
const User = require('../models/User')

module.exports.categoryCheck = (params) => {
    return Transaction.find({value: params.value, userRef: params.userRef}).then(result => {
        return result.length > 0 ? true : false
    })
}


module.exports.addCategory = (params) => {
    let category = new Transaction({
        userRef: params.userRef,
        categoryType: params.body.categoryType,
        name: params.body.name,
        value: params.body.value,
        isDebit: params.body.isDebit
    })

    return category.save().then((category, err) => {
        return (err) ? 'saved' : true
    })
}


module.exports.addSubCategoryCheck = (params) => {
    return Transaction.find({value: params.body.mainCatValue, userRef: params.userRef}).then(data => {
        // return data[0].categorySelection.length
        if (data[0].categorySelection.length > 0){
            let dataLength = data[0].categorySelection.length            
            for (num=0;num<dataLength;num++){
                if (data[0].categorySelection[num].value === params.body.value){
                    return true
                } 
            }
            return false
        } else{
            return false
        } 
    })
}


module.exports.addSubCategory = (params) => {
    let subCategory = {
        categoryType: params.body.categoryType,
        name: params.body.name,
        value: params.body.value
    }
    return Transaction.find({value: params.body.mainCatValue, userRef: params.userRef}).then(data => {
        data[0].categorySelection.push(subCategory)
        return data[0].save().then((data, err) => {
            return (err) ? 'saved' : true
        })
    })

}


module.exports.getMain = (params) => {
    return Transaction.find({userRef: params.userRef}).then(user => {
        user.password = undefined
        return user
    })
}


module.exports.delete = (params) => {
    const updates = { isActive: params.isActive }
    return Transaction.findOneAndUpdate({value: params.value, userRef: params.userRef}, updates).then((doc, err) => {
        return (err) ? false : true
    })
}

module.exports.deleteSubCat = (params) => {
    // const updates = { isActive: params.isActive }
    return Transaction.find({value: params.mainCatValue, userRef: params.userRef}).then(data => {
        let dataLength = data[0].categorySelection.length            
        for (num=0;num<dataLength;num++){
            if (data[0].categorySelection[num].value === params.value){
                data[0].categorySelection[num].isActive = params.isActive
                return data[0].save().then((data,err) => {
                    return (err) ? false : true
                })
            } 
        }
        
    })
}


// module.exports.deleteSubCat = (params) => {
//     // const updates = { isActive: params.isActive }
//     return Transaction.find({value: params.mainCatValue, userRef: params.userRef}).then(data => {
//         let dataLength = data[0].categorySelection.length            
//         for (num=0;num<dataLength;num++){
//             if (data[0].categorySelection[num].value === params.value){
//                 data[0].categorySelection[num].isActive = params.isActive
//                 return 'updated'
//             } 
//         }
//     })
// }