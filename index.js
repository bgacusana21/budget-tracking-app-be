const express = require('express')
const app = express()
require('dotenv').config()
const cors = require('cors')
const mongoose = require('mongoose')
const port = process.env.PORT
const mongodbCloud = process.env.DB_MONGODB
const auth = require('./auth')

const userRoutes = require('./routes/user')
const transactionRoutes = require('./routes/transaction')

const corsOptions = {
	origin: ['http://localhost:3000', 'https://www.google.com/' ],
	optionsSuccessStatus: 200//for compatibility with older browsers
}

app.use(express.json({limit: '5mb'}))
app.use(express.urlencoded({extended: true}))
app.use(cors())
app.use('/api/transactions', transactionRoutes)
app.use('/api/users', userRoutes)


mongoose.connection.once('open', () => {console.log('Connected to MongoDB Atlas')})
mongoose.connect(mongodbCloud, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})

app.listen(port || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`)
})





