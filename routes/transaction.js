const express = require('express');
const { reset } = require('nodemon');
const router = express.Router()
const auth = require('../auth');
const TransactionController = require('../controllers/transaction');
const { register } = require('../controllers/user');


//=============================================================
//Used to add main category
//integrated router to controller to check if category is existing
//=============================================================
router.post('/:catType', auth.verify, (req, res) => {

    const params = {
        userRef: auth.decode(req.headers.authorization).id,
        body: req.body,
        catType: req.params.catType
    }

    if (params.catType === 'mainCategory') {
        TransactionController.categoryCheck(params).then(result => {
            if (result) {
                res.send(false)
            } else {
                TransactionController.addCategory(params).then(result => {
                    res.send(result)
                })
            }
        })
    } else if (params.catType === 'subCategory'){
        TransactionController.addSubCategoryCheck(params).then(result => {
            // result > 0 ? res.send('more than 1') : res.send('0')
            // res.send(result)
            if (result) {
                res.send(false)
            } else {
                TransactionController.addSubCategory(params).then(result => {
                    res.send(result)
                })
            }
        })
    } else res.send('check category type')

})



//=============================================================
//Used to fetch the data(object) for the main categories
//=============================================================
router.get('/:catType', auth.verify, (req,res) => {
    const params = {
        userRef: auth.decode(req.headers.authorization).id,
        catType: req.params.catType        
    }
    if (params.catType == 'mainCategory'){
        TransactionController.getMain(params).then(user => {
            res.send(user)
        })
    } else if (params.catType == 'subCategory'){
        res.send(params)
    } else res.send('check category type')


})

//=============================================================
//Used to set category status isActive: true or false
//=============================================================
router.delete('/:value/:mainCatValue', auth.verify, (req,res) => {
  
    const params = {
        mainCatValue: req.params.mainCatValue,
        value: req.params.value,
        userRef: auth.decode(req.headers.authorization).id,
        isActive: req.body.isActive, 
    }
    if(params.mainCatValue==='main'){
        TransactionController.delete(params).then(result => res.send(result))
    } else{
        TransactionController.deleteSubCat(params).then(result => res.send(result))
    }
})

module.exports = router



// //=============================================================
// //WORKING FETCH FOR MAIN CATEGORY
// //Used to add main category
// //integrated router to controller to check if category is existing
// //=============================================================
// router.post('/', auth.verify, (req, res) => {

//     const params = {
//         value: req.body.value,
//         userRef: auth.decode(req.headers.authorization).id,
//         body: req.body
//     }

//     TransactionController.categoryCheck(params).then(result => {
//         if (result) {
//             res.send(false)
//         } else {
//             TransactionController.addCategory(params).then(result => {
//                 res.send(result)
//             })
//         }
//     })
// })

// //=============================================================
// //Used to add sub category
// //integrated router to controller to check if category is existing
// //=============================================================
// router.post('/:mainCat', auth.verify, (req, res) => {
//     const params = {
//         userRef: auth.decode(req.headers.authorization).id,
//         value: req.body.value,
//         searchMainCat: req.params.mainCat,
//         body: req.body
//     }

//     TransactionController.subCategoryCheck(params).then(result => {
//         if (result === false){
//             TransactionController.addSubCategory(params).then(result => {
//                 res.send(result)
//             })
//         }   else {
//                 result.find({value: params.value})
//                     .then(res => res.length > 0 ? 'do not add' : 'add')
//             // {result.value === params.value? res.send('do not add') : res.send('add')}
//         }
           

//         // if (result) {
//         //     res.send(false)
//         // } else {
//         //     TransactionController.addSubCategory(params).then(result => {
//         //         res.send(result)
//         //     })
//         // }
//     })
// })