const express = require('express')
const router = express.Router()
const auth = require('../auth');
const UserController = require('../controllers/user');
const User = require('../models/User');

router.post('/', (req, res) => {
    UserController.register(req.body).then(result => {
        res.send(result)
    })
})

router.post('/change', auth.verify, (req,res) => {
    const params = {
        userRef: auth.decode(req.headers.authorization).id,
        oldPassword: req.body.oldPassword,
        newPassword: req.body.newPassword
    }

    UserController.change(params).then(result => {
        res.send(result)
    })
})

router.post('/email-exists', (req, res) => {
    UserController.emailExists(req.body).then(result => res.send(result))
})

router.post('/verify-google-id-token', async (req, res) => {
    res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})

router.post('/login', (req,res) => {
    UserController.login(req.body).then(result => {
        res.send(result)
    })
})

router.get('/details', auth.verify, (req,res) => {
    const user = auth.decode(req.headers.authorization)
    UserController.get({ userId: user.id}).then(user => {
        res.send(user)
    })
})

router.post('/transactions/add', auth.verify, (req, res) => {
    const params = {
        userRef: auth.decode(req.headers.authorization).id,
        body: {
            transactionType: req.body.transactionType,
            transactSubCat: req.body.transactSubCat,
            summary: req.body.summary,
            description: req.body.description,
            amount: req.body.amount,
            transactionTimestamp: req.body.transactionTimestamp
        }
    }

    UserController.addTransaction(params).then(result => {
        res.send(result)
    })

})

router.get('/transactions/history', auth.verify, (req,res) => {
    const params = {userRef: auth.decode(req.headers.authorization).id}

    UserController.transactions(params).then(result => {
        res.send(result)
    })

})

module.exports = router